*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${LOGIN URL}    https://test.blackdale.eu/
${BROWSER}      Firefox
${OUTPUT DIR}   home/paolo/Pulpit/robot/logs/

*** Test Cases ***
Open Browser
    Open main page

Valid Login 
     Open Offer 
     Open Jewellery 
     Open Ring 
     Change Value
     Send Form

*** Keywords ***
Open main page
     Open Browser    ${LOGIN URL}   ${BROWSER}
     Title Should Be    Sklep testowy 
     Click Element   xpath=//*[@id="cookie_action_close_header"] 
Open Offer
     Click Element   xpath=/html/body/div[1]/div/div/div/div/div/section[5]/div/div/div/div/div/section[2]/div[2]/div/div[1]/div/div/div[5]/div/div/a  #wybranie skorzystajz oferty
Open Jewellery
     Click Element   xpath=/html/body/div[1]/div/div/div[1]/div/div[2]/ul/li[2]/a  #zakładka biżuteria
Open Ring   
     Click Element   xpath=/html/body/div[1]/div/div/div[2]/main/div/ul/li[2]/div[2]/a[1]/h2    #wybór opcji "pierścionek"
Change Value
     Click Element   xpath=//*[@id="plus_qty"]
     Click Element   xpath=//*[@id="plus_qty"]
     Click Element   xpath=//*[@id="plus_qty"]
     Click Element   xpath=//*[@id="plus_qty"]
     Click Element   xpath=/html/body/div[1]/div/div/div/main/div/div[2]/div[2]/form/button
     Click Element   xpath=/html/body/div[1]/header/div[2]/div/div/div/div/div[3]/div/nav/div/ul/li[6]/a/span[2]
Send Form
     Input Text      id=wpforms-2596-field_0  Pawel
     Input Text      id=wpforms-2596-field_4  www@poczta.com
     Input Text      id=wpforms-2596-field_2  Lorem Ipsum
     Click Element   xpath=//*[@id="wpforms-submit-2596"]
