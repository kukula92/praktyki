*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${LOGIN URL}    https://test.blackdale.eu/
${BROWSER}      Firefox

*** Test Cases ***
Open Browser
    Open main page

Valid Login 
     Open Login Page 
     Input Login 
     Input Password 
     Login Button 

*** Keywords ***
Open main page
    Open Browser    ${LOGIN URL}   ${BROWSER}
    Title Should Be    Sklep testowy      
Open Login Page
     Click Element   xpath=/html/body/div[1]/header/div[2]/div/div/div/div/div[3]/div/nav/div/ul/li[5]/a/span[2]  #wybranie elementu logowania
Input Login
     Input Text   id=username   test@test.pl  #wprowadzenie loginu 
Input Password   
     Input Text   id=password   Test1234    #wprowadzenie hasła
Login Button
     Click Element   xpath=/html/body/div[1]/div/div/div/main/article/div/div/form/p[3]/button  #zatwierdzenie logowania
